import { SalesDataService } from './../../shared/services/sales-data.service';
import { Customer } from './../../shared/models/customer';
import { Order } from './../../shared/models/order';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-section-orders',
  templateUrl: './section-orders.component.html',
  styleUrls: ['./section-orders.component.css']
})
export class SectionOrdersComponent implements OnInit {

  constructor(private _salesService: SalesDataService) { }

  total = 0;
  page = 1;
  limit = 10;
  loading = false;
  orders: Order[];

  ngOnInit() {
    this.getOrders();
  }
  goToPrevious() {
    this.page--;
    this.getOrders();
  }
  goToNext() {
    this.page++;
    this.getOrders();
  }
  goToPage(n: number): void {
    this.page = n;
    this.getOrders();
  }
  getOrders(): void {
    this._salesService.getOrders(this.page, this.limit).subscribe(
      res => {
        this.orders = res['page']['data'];
        this.total = res['page'].total;
        this.loading = false;
      }
    );
  }

}
