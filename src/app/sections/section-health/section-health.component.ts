
import { Observable } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ServerService } from 'src/app/shared/services/server.service';
import { ServerMessage } from 'src/app/shared/server-message';



// const SAMPLE_SERVERS = [
//   { id: 1, name: 'dev-web', isOnline: true },
//   { id: 2, name: 'backend', isOnline: false },
//   { id: 3, name: 'microservice', isOnline: false },
//   { id: 4, name: 'email', isOnline: true },
// ];
@Component({
  selector: 'app-section-health',
  templateUrl: './section-health.component.html',
  styleUrls: ['./section-health.component.css']
})
export class SectionHealthComponent implements OnInit, OnDestroy {

  constructor(private _serverService: ServerService) { }
  timerSubscription;
  servers;
  ngOnInit() {
    this.refreshData();
  }
  ngOnDestroy() {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
  }
  refreshData() {
    this._serverService.getServers().subscribe(res => {
      this.servers = res;
    });
    this.subscribeToData();
  }
  subscribeToData() {
    // this.timerSubscription = Observable.timer(5000).first().subscribe(() => this.refreshData());
  }
  sendMessage(msg: ServerMessage) {
    this._serverService.handleServerMessage(msg).subscribe(res => console.log(res));
  }

}
