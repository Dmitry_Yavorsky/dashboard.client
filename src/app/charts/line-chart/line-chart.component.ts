import { LINE_CHART_COLORS } from './../../shared/chart.color';
import { Component, OnInit } from '@angular/core';
import { SalesDataService } from 'src/app/shared/services/sales-data.service';
import * as moment from 'moment';


// const LINE_CHART_SAMPLE_DATA: any[] = [
//   { data: [32, 14, 46, 23, 38, 56], label: 'Sentiment Analysis' },
//   { data: [52, 24, 56, 26, 31, 53], label: 'Image Recognition' },
//   { data: [12, 164, 66, 43, 28, 51], label: 'Forecasting' }
// ];

const LINE_CHART_LABELS: string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'];
@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {

  constructor(private _salesService: SalesDataService) { }
  topCustomers: string[];
  allOrders: any[];
  lineChartData: any;
  lineChartLabels: any;
  lineChartOptions: any = {
    responsive: true
  };
  lineChartColors = LINE_CHART_COLORS;
  lineChartLegend: true;
  lineChartType = 'line';

  ngOnInit() {
    this._salesService.getOrders(1, 100).subscribe(res => {
      this.allOrders = res['page']['data'];
      this._salesService.getOrdersByCustomer(3).subscribe((rec: any[]) => {
        this.topCustomers = rec.map(x => x['name']);
        const allChartData = this.topCustomers.reduce((result, i) => {
          result.push(this.getChartData(this.allOrders, i));
          return result;
        }, []);
        let dates = allChartData.map(x => x['data']).reduce((a, i) => {
          a.push(i.map(o => new Date(o[0])));
          return a;
        }, []);
        dates = [].concat.apply([], dates);
        const r = this.getCustomerOrdersByDate(allChartData, dates)['date'];
        console.log(r);
        this.lineChartLabels = r[0]['orders'].map(o => o['date']);

        this.lineChartData = [
          { 'data': r[0].orders.map(x => x.total), 'label': r[0].customer },
          { 'data': r[1].orders.map(x => x.total), 'label': r[1].customer },
          { 'data': r[2].orders.map(x => x.total), 'label': r[2].customer },
        ];
 
      });
    });
  }
  getCustomerOrdersByDate(orders: any, dates: any) {
    const customers = this.topCustomers;
    const prettyDates = dates.map(x => this.toFriendlyDate(x));
    const u = Array.from(new Set(prettyDates)).sort();

    const result = {};
    const dataSets = result['date'] = [];

    customers.reduce((r, y, i) => {
      const customerOrders = [];
      dataSets[i] = {
        customer: y,
        orders: u.reduce((f, e, j) => {
          const obj = {};
          obj['date'] = e;
          obj['total'] = this.getCustomerDateTotal(e, y);
          customerOrders.push(obj);
          return customerOrders;
        })
      };
      return r;
    }, []);
    return result;
  }
  getCustomerDateTotal(date: any, customer: string) {
    const r = this.allOrders.filter(o => o.customer.name === customer && this.toFriendlyDate(o.placed) === date);
    const result = r.reduce((a, b) => {
      return a + b.total;
    }, 0);
    return result;
  }
  toFriendlyDate(date: Date) {
    return moment(date).endOf('day').format('YY-MM-DD');

  }
  getChartData(allOrders: any, name: string) {
    const customerOrders = allOrders.filter(o => o.customer.name === name);

    const formattedOrders = customerOrders.reduce((r, e) => {
      r.push([e.placed, e.total]);
      return r;
    }, []);
    return { customer: name, data: formattedOrders };
  }

}
