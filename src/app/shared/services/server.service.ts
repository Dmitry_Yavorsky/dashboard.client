import { RequestOptions } from 'https';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { ServerMessage } from '../server-message';


@Injectable({
  providedIn: 'root'
})
export class ServerService {

  constructor(private httpClient: HttpClient) {

  }
  options: RequestOptions;
  headers: Headers;
  getServers() {
    return this.httpClient.get('http://localhost:51346/api/server').pipe(catchError(this.handleError));
  }
  private handleError(error: any) {
    const errMsg = (error.message) ? error.message : error.status ? '${error.status} - ${error.statusText}' : 'Server error';
    return Observable.throw(errMsg);
  }
  handleServerMessage(msg: ServerMessage) {
    const url = 'http://localhost:51346/api/server' + msg.id;
    return this.httpClient.put(url, msg);
  }
}
