
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SalesDataService {

  constructor(private _http: HttpClient) {
  }
  getOrders(pageIndex: number, pageSize: number) {
    return this._http.get('http://localhost:51346/api/order/' + pageIndex + '/' + pageSize);
  }
  getOrdersByCustomer(n: number) {
    return this._http.get('http://localhost:51346/api/order/bycustomer/' + n);
  }
  getOrdersByState() {
    return this._http.get('http://localhost:51346/api/order/bystate/');
  }
}
